﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using VacationDays;

namespace VacationDaysIntegrationTests
{
    public class TestClientProvider : IDisposable
    {
        private readonly TestServer server;

        public HttpClient Client { get; set; }

        private string connectionString = "Server=(localdb)\\mssqllocaldb;Database=VacationDays;Trusted_Connection=True;MultipleActiveResultSets=true";

        public TestClientProvider()
        {
            server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
                .UseSetting("ConnectionStrings:DefaultConnection", connectionString));
            Client = server.CreateClient();
        }

        public void Dispose()
        {
            // if server != null, then dispose
            server?.Dispose();
            Client?.Dispose();
        }
    }
}
