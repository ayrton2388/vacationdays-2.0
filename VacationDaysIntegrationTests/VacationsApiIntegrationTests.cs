using System;
using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Mvc.Testing;
using VacationDays;
using System.Net;
using FluentAssertions;
using VacationDays.Models;
using Newtonsoft.Json;
using System.Text;

namespace VacationDaysIntegrationTests
{
    public class VacationsApiIntegrationTests
    {
        [Fact]
        public async Task NoConditionSuccess_NoCondition_Success()
        {
            using (var client = new TestClientProvider().Client)
            { 
                var response = await client.GetAsync("home/NoConditionSuccess");
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                Assert.Equal("No Condition Success", responseString);
            }
        }

        [Fact]
        public async Task Get_All()
        {
            using (var client = new TestClientProvider().Client)
            { 
                var response = await client.GetAsync("/api/VacationsApi");
                response.EnsureSuccessStatusCode();

                // use fluentAssertions instead of regular mode
                response.StatusCode.Should().Be(HttpStatusCode.OK); 

                //regular mode
                //Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Fact]
        public async Task Get_ID()
        {
            using (var client = new TestClientProvider().Client)
            {
                var response = await client.GetAsync("/api/VacationsApi");
                response.EnsureSuccessStatusCode();

                response.StatusCode.Should().Be(HttpStatusCode.OK);
            } 
        }

        //[Fact]
        //public async Task Create()
        //{
        //    using (var client = new TestClientProvider().Client)
        //    {
        //        Vacation vacation = new Vacation();
        //        vacation.StartDate = DateTime.Today;
        //        vacation.EndDate = DateTime.Today;
        //        vacation.DaysTaken = 1;
        //        vacation.Status = VacationDays.Classes.VacationStatus.Pending;
        //        vacation.OwnerId = "Test_Create User";

        //        var response = await client.PostAsync("/api/VacationsApi", new StringContent(
        //            JsonConvert.SerializeObject(vacation), Encoding.UTF8, "application/json"));

        //        response.EnsureSuccessStatusCode();
        //        response.StatusCode.Should().Be(HttpStatusCode.OK);
        //    }
        //} 
    } 
} 
  