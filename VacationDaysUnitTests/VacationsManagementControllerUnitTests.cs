﻿using Moq;
using VacationDays.Services;
using VacationDays.Controllers;
using VacationDays.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using System.Collections.Generic;
using FluentAssertions;
using System.Linq;
using VacationDays.ViewModels.Vacations;
using System;
using VacationDays.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace VacationDaysUnitTests
{
    public class FakeUserManager : UserManager<IdentityUser>
    {
        public FakeUserManager()
            : base(new Mock<IUserStore<IdentityUser>>().Object,
                  new Mock<IOptions<IdentityOptions>>().Object,
                  new Mock<IPasswordHasher<IdentityUser>>().Object,
                  new IUserValidator<IdentityUser>[0],
                  new IPasswordValidator<IdentityUser>[0],
                  new Mock<ILookupNormalizer>().Object,
                  new Mock<IdentityErrorDescriber>().Object,
                  new Mock<IServiceProvider>().Object,
                  new Mock<ILogger<UserManager<IdentityUser>>>().Object)
        { }
    }    

    public class VacationsManagementControllerUnitTests
    {
        Mock<IVacationData> vacationServiceMock = new Mock<IVacationData>();
        Mock<IUserInfo> userInfoMock = new Mock<IUserInfo>();
        Mock<INationalFreeDaysData> nationalFreeDaysMock = new Mock<INationalFreeDaysData>();
        DateHelper dateHelper;
        Mock<FakeUserManager> userManagerMock = new Mock<FakeUserManager>();
        Mock<IEmailService> emailServiceMock = new Mock<IEmailService>(); 
         
        private void InitializeMocks() 
        {   
            vacationServiceMock = new Mock<IVacationData>();
            userInfoMock = new Mock<IUserInfo>();
            nationalFreeDaysMock = new Mock<INationalFreeDaysData>();          
            userManagerMock = new Mock<FakeUserManager>();
            dateHelper = new DateHelper(nationalFreeDaysMock.Object);
            emailServiceMock = new Mock<IEmailService>();
        }  

        [Fact]
        public void Index_NoCondition_ReturnsAllVacations()
        {
            InitializeMocks();

            vacationServiceMock.Setup(x => x.GetAll()).Returns(() => new List<Vacation>
            {
                new Vacation { Id = 1, OwnerId = "user1" },
                new Vacation { Id = 2, OwnerId = "user1" },
                new Vacation { Id = 3, OwnerId = "user2" },
            });

            var user1 = new IdentityUser() { Id = "user1", UserName = "user1" };
            var user2 = new IdentityUser() { Id = "user2", UserName = "user2" };
            var users = new List<IdentityUser>() { user1, user2 }.AsQueryable();

            userManagerMock.Setup(x => x.Users).Returns(users);

            var vacationsManagementController = new VacationsManagementController(vacationServiceMock.Object,
                userInfoMock.Object, userManagerMock.Object, dateHelper, emailServiceMock.Object);

            var result = vacationsManagementController.Index();

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<VacationManagementOutputModel>(viewResult.ViewData.Model);

            model.vacationsList.Count().Should().Be(3);
        }

        [Fact]
        public void Pending_NoCondition_ReturnsAllPendingVacations()
        {
            InitializeMocks();

            vacationServiceMock.Setup(x => x.GetAll()).Returns(() => new List<Vacation>
            {
                new Vacation { Id = 1, OwnerId = "user1", Status=VacationDays.Classes.VacationStatus.Pending },
                new Vacation { Id = 2, OwnerId = "user1", Status=VacationDays.Classes.VacationStatus.Approved },
                new Vacation { Id = 3, OwnerId = "user2", Status=VacationDays.Classes.VacationStatus.Pending },
            });

            var user1 = new IdentityUser() { Id = "user1", UserName = "user1" };
            var user2 = new IdentityUser() { Id = "user2", UserName = "user2" };
            var users = new List<IdentityUser>() { user1, user2 }.AsQueryable();

            userManagerMock.Setup(x => x.Users).Returns(users);

            var vacationsManagementController = new VacationsManagementController(vacationServiceMock.Object,
                userInfoMock.Object, userManagerMock.Object, dateHelper, emailServiceMock.Object);

            var result = vacationsManagementController.Pending();

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<VacationManagementOutputModel>(viewResult.ViewData.Model);

            model.vacationsList.Count().Should().Be(2); 
        }

        [Fact]
        public void Details_GoodIdPassed_ReturnsView()
        {
            InitializeMocks();

            Vacation vacation = new Vacation() { Id = 900 };
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(vacation);

            var vacationsManagementController = new VacationsManagementController(vacationServiceMock.Object,
                userInfoMock.Object, userManagerMock.Object, dateHelper, emailServiceMock.Object);

            var result = vacationsManagementController.Details(244);

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<Vacation>(viewResult.ViewData.Model);

            model.Id.Should().Be(vacation.Id); 
        }
        [Fact]
        public void Details_NonExistingIdPassed_ReturnsNotFound()
        {
            InitializeMocks();

            Vacation vacation = new Vacation() { Id = 900 };
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns((Vacation)null);

            var vacationsManagementController = new VacationsManagementController(vacationServiceMock.Object,
                userInfoMock.Object, userManagerMock.Object, dateHelper, emailServiceMock.Object);

            var result = vacationsManagementController.Details(244);

            //assert
            var viewResult = result.Should().BeOfType<NotFoundObjectResult>().Subject; 
        }

    }
} 

 