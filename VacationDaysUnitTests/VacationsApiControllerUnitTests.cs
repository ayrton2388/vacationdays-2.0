﻿using Moq;
using VacationDays.Services;
using VacationDays.Controllers;
using VacationDays.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using System.Collections.Generic;
using FluentAssertions;
using System.Linq;

namespace VacationDaysUnitTests
{
    public class VacationsApiControllerUnitTests
    {
        [Fact]
        public void GetAll_NoCondition_ReturnsAllVacations()
        {
            // arrange
            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.GetAll()).Returns(() => new List<Vacation>
            {
                new Vacation { Id = 1, OwnerId = "1" },
                new Vacation { Id = 2, OwnerId = "2" },
                new Vacation { Id = 3, OwnerId = "3" },
            });

            var controller = new VacationsApiController(vacationServiceMock.Object);

            // act
            var result = controller.GetAllVacations();

            //assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var vacations = okResult.Value.Should().BeAssignableTo<IEnumerable<Vacation>>().Subject;
            vacations.Count().Should().Be(3);
        }

        [Fact]
        public void Get_IdPassed_ReturnsProperVacation()
        {
            // arrange
            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(
                () => new Vacation { Id = 66, OwnerId = "3" }); 

            var controller = new VacationsApiController(vacationServiceMock.Object);

            // act
            var result = controller.GetVacation(1);
            
            //assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var vacation = okResult.Value.Should().BeAssignableTo<Vacation>().Subject;
            vacation.Id.Should().Be(66);   
        }

        [Fact]
        public void Get_IdNotFound_ReturnsEmptyResponsVacation()
        {
            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns((Vacation)null);
            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.GetVacation(5);

            var okResult = result.Should().BeOfType<NotFoundResult>().Subject;       
        }

        [Fact]
        public void Create_VacationPassed_ReturnsOk()
        {
            var vacation = new Vacation();
            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Add(It.IsAny<Vacation>())).Returns(vacation);
            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.Create(vacation);

            var okResult = result.Should().BeOfType<OkResult>().Subject;
        }

        [Fact]
        public void CreateAndReturn_VacationPassed_ReturnsOk()
        {
            var vacation = new Vacation() { Id = 55}; 
            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Add(It.IsAny<Vacation>())).Returns(vacation);
            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.CreateAndReturn(vacation);

            //assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var vacationRes = okResult.Value.Should().BeAssignableTo<Vacation>().Subject;
            vacationRes.Id.Should().Be(55); 
        }

        [Fact]
        public void Create_NullPassed_BadResponseReturned()
        {
            var vacation = new Vacation();

            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Add(It.Is<Vacation>(y => y == vacation))).Returns(vacation);

            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.Create(null);

            var okResult = result.Should().BeOfType<BadRequestResult>().Subject; 
        }

        [Fact]
        public void Delete_GoodIdPassed_ReturnsOk() 
        {
            var vacationServiceMock = new Mock<IVacationData>();

            // the DeleteVacation function uses Get(id), too, so we have to mock that too.
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(
                () => new Vacation { Id = 33, OwnerId = "#33" });
            vacationServiceMock.Setup(x => x.Remove(It.IsAny<int>())); 

            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.DeleteVacation(33); 

            var okResult = result.Should().BeOfType<OkResult>().Subject;
        }


        [Fact]
        public void Delete_BadIdPassed_NotFoundReturned()
        {
            var vacationServiceMock = new Mock<IVacationData>();

            // the DeleteVacation function uses Get(id), too, so we have to mock that too.
            // null has to be casted to the right type
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns((Vacation)null);
            vacationServiceMock.Setup(x => x.Remove(It.IsAny<int>()));
             
            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.DeleteVacation(33);

            var okResult = result.Should().BeOfType<NotFoundResult>().Subject;
        }

         
        // tomorrow: test MVC controller!  "View" inherits from "actionResult", so i might
        // be able to get the return object? Look into it.
    }
}
 