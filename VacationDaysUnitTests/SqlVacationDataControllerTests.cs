﻿using System;
using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Mvc.Testing;
using VacationDays;
using Moq;
using VacationDays.Services;
using VacationDays.Controllers;
using VacationDays.Models;
using Microsoft.AspNetCore.Mvc;

namespace VacationDaysUnitTests
{
    public class SqlVacationDataControllerTests
    {
        [Fact]
        public void GetAll_NoCondition_ReturnsAllVacations()
        {
            var vacationServiceMock = new Mock<IVacationData>();
            var controller = new VacationsApiController(vacationServiceMock.Object);

            controller.GetAllVacations();
            vacationServiceMock.Verify(x => x.GetAll());    
        }

        [Fact]
        public void Get_IdPassed_ReturnsProperVacation()
        {
            var vacation = new Vacation();

            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(vacation);

            var controller = new VacationsApiController(vacationServiceMock.Object);
            var result = controller.GetVacation(-23);

            Assert.Equal((result as ObjectResult)?.Value, vacation);
        }

        [Fact]
        public void Get_NoRequestedVacation_ReturnsEmptyResponsVacation()
        {
            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns((Vacation)null);

            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.GetVacation(5);  

            Assert.True(result is NotFoundResult);
        }

        [Fact]
        public void Create_VacationPassed_ProperResponseReturned()
        {
            var vacation = new Vacation();

            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Add(It.Is<Vacation>(y => y == vacation))).Returns(vacation);

            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.Create(vacation);

            vacationServiceMock.Verify(x => x.Add(It.Is<Vacation>(y => y == vacation)));
            Assert.True(result is OkResult); 
        }

        [Fact]
        public void CreateAndReturn_VacationPassed_ProperResponseReturned()
        {
            var vacation = new Vacation();

            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Add(It.Is<Vacation>(y => y == vacation))).Returns(vacation);

            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.CreateAndReturn(vacation);

            vacationServiceMock.Verify(x => x.Add(It.Is<Vacation>(y => y == vacation)));

            Assert.Equal((result as ObjectResult)?.Value, vacation);
        }

        [Fact]
        public void Create_NullPassed_BadResponseReturned()
        {
            var vacation = new Vacation();

            var vacationServiceMock = new Mock<IVacationData>();
            vacationServiceMock.Setup(x => x.Add(It.Is<Vacation>(y => y == vacation))).Returns(vacation);

            var apiController = new VacationsApiController(vacationServiceMock.Object);

            var result = apiController.Create(null);

            Assert.True(result is BadRequestResult);
        }

        //[Fact]
        //public void Update_AudioBookPassed_ReturnedProperAudioBook()
        //{
        //    var id = Guid.NewGuid();
        //    var audioBook = new AudioBook()
        //    {
        //        Id = id
        //    };

        //    var audioServiceMock = new Mock<IAudioBookService>();
        //    audioServiceMock.Setup(x => x.Update(It.IsAny<Guid>(), It.Is<AudioBook>(ab => ab == audioBook))).Returns(audioBook);

        //    var apiController = new AudioBookApiController(audioServiceMock.Object);

        //    var result = apiController.Update(id, audioBook);

        //    audioServiceMock.Verify(x => x.Update(It.Is<Guid>(guid => guid == id), It.Is<AudioBook>(ab => ab == audioBook)));
        //    Assert.True(result is OkResult);
        //}

        //[Fact]
        //public void Update_WrongIDPassed_BadRequestReturned()
        //{
        //    var audioBook = new AudioBook();
        //    var id = Guid.NewGuid();

        //    var audioServiceMock = new Mock<IAudioBookService>();
        //    audioServiceMock.Setup(x => x.Update(It.IsAny<Guid>(), It.Is<AudioBook>(ab => ab == audioBook))).Returns(audioBook);

        //    var apiController = new AudioBookApiController(audioServiceMock.Object);

        //    var result = apiController.Update(id, audioBook);

        //    Assert.True(result is BadRequestResult);
        //}

        //[Fact]
        //public void Update_NullPassed_BadRequestReturned()
        //{
        //    var audioBook = new AudioBook();
        //    var id = Guid.NewGuid();

        //    var audioServiceMock = new Mock<IAudioBookService>();
        //    audioServiceMock.Setup(x => x.Update(It.IsAny<Guid>(), It.Is<AudioBook>(ab => ab == audioBook))).Returns(audioBook);

        //    var apiController = new AudioBookApiController(audioServiceMock.Object);

        //    var result = apiController.Update(id, null);

        //    Assert.True(result is BadRequestResult);
        //}

        //[Fact]
        //public void Update_ExceptionTrowed_NotFoundReturned()
        //{
        //    var id = Guid.NewGuid();
        //    var audioBook = new AudioBook()
        //    {
        //        Id = id
        //    };

        //    var audioServiceMock = new Mock<IAudioBookService>();
        //    audioServiceMock
        //        .Setup(x => x.Update(It.IsAny<Guid>(), It.Is<AudioBook>(ab => ab == audioBook)))
        //        .Throws(new Exception());

        //    var apiController = new AudioBookApiController(audioServiceMock.Object);

        //    var result = apiController.Update(id, audioBook);

        //    Assert.True(result is NotFoundResult);
        //}

        //[Fact]
        //public void Delete_GoodIdPassed_ProperFunctionsCalled()
        //{
        //    var id = Guid.NewGuid();

        //    var audioServiceMock = new Mock<IAudioBookService>();
        //    audioServiceMock.Setup(x => x.Delete(It.IsAny<Guid>()));

        //    var apiController = new AudioBookApiController(audioServiceMock.Object);

        //    var result = apiController.Delete(id);

        //    audioServiceMock.Verify(x => x.Delete(It.Is<Guid>(guid => guid == id)));
        //    Assert.True(result is OkResult);
        //}

        //[Fact]
        //public void Delete_BadIdPassed_NotFoundReturned()
        //{
        //    var id = Guid.NewGuid();

        //    var audioServiceMock = new Mock<IAudioBookService>();
        //    audioServiceMock.Setup(x => x.Delete(It.IsAny<Guid>())).Throws(new Exception());

        //    var apiController = new AudioBookApiController(audioServiceMock.Object);

        //    var result = apiController.Delete(id);

        //    audioServiceMock.Verify(x => x.Delete(It.Is<Guid>(guid => guid == id)));
        //    Assert.True(result is NotFoundResult);
        //}
    }  
}  
