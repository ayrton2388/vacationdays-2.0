﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VacationDays.Controllers;
using VacationDays.Data;
using VacationDays.Models;
using VacationDays.Services;
using Xunit;

namespace VacationDaysUnitTests
{
    public class NationalFreeDaysControllerUnitTests
    {
        [Fact]
        public void Index_NoCondition_ReturnsAllVacations()
        {
            // arrange
            var nationalFreeDaysData = new Mock<INationalFreeDaysData>();

            nationalFreeDaysData.Setup(x => x.GetAll()).Returns(() => new List<NationalFreeDay>
            {
                new NationalFreeDay {  Id = 1, Date = DateTime.Now.Date.AddDays(-1), },
                new NationalFreeDay {  Id = 2, Date = DateTime.Now.Date, },
                new NationalFreeDay {  Id = 3, Date = DateTime.Now.Date.AddDays(+1), },
            }); 

            var controller = new NationalFreeDaysController(nationalFreeDaysData.Object);

            // act 
            var result = controller.Index();

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject; 
            var model = Assert.IsAssignableFrom<IEnumerable<NationalFreeDay>>(viewResult.ViewData.Model);

            model.Count().Should().Be(3);
        }

        [Fact]
        public void AddFreeDay_GoodInput_ReturnRedirectToAction()
        {           
            NationalFreeDay freeDay = new NationalFreeDay();
            freeDay.Date = DateTime.Now.Date;

            var nationalFreeDaysData = new Mock<INationalFreeDaysData>();
            nationalFreeDaysData.Setup(x => x.Add(It.IsAny<NationalFreeDay>())).Returns(freeDay);

            var controller = new NationalFreeDaysController(nationalFreeDaysData.Object);

            // act 
            var result = controller.AddFreeDay(freeDay);

            //assert
            var viewResult = result.Should().BeOfType<RedirectToActionResult>().Subject; 
        }

        [Fact]
        public void AddFreeDay_DateNotSet_ReturnErrorView()
        {
            NationalFreeDay freeDay = new NationalFreeDay();
            freeDay.Date = DateTime.MinValue;

            var nationalFreeDaysData = new Mock<INationalFreeDaysData>();
            nationalFreeDaysData.Setup(x => x.Add(It.IsAny<NationalFreeDay>())).Returns(freeDay);

            var controller = new NationalFreeDaysController(nationalFreeDaysData.Object);

            // act 
            var result = controller.AddFreeDay(freeDay);

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<IEnumerable<string>>(viewResult.ViewData.Model);

            model.Count().Should().BeGreaterThan(0); 
        }

        [Fact]
        public void AddFreeDay_NullInput_ReturnErrorView()
        {
            var nationalFreeDaysData = new Mock<INationalFreeDaysData>();
            nationalFreeDaysData.Setup(x => x.Add(It.IsAny<NationalFreeDay>())).Returns((NationalFreeDay)null);

            var controller = new NationalFreeDaysController(nationalFreeDaysData.Object);

            // act 
            var result = controller.AddFreeDay((NationalFreeDay)null);

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<IEnumerable<string>>(viewResult.ViewData.Model);

            model.Count().Should().BeGreaterThan(0);
        }

        [Fact]
        public void AddFreeDay_DateAlreadyExists_ReturnErrorView()
        {
            NationalFreeDay freeDay = new NationalFreeDay();
            freeDay.Date = DateTime.MinValue;

            var nationalFreeDaysData = new Mock<INationalFreeDaysData>();
            nationalFreeDaysData.Setup(x => x.Add(It.IsAny<NationalFreeDay>())).Returns(freeDay);
            nationalFreeDaysData.Setup(x => x.IsNationalFreeDay(It.IsAny<DateTime>())).Returns(true);
            var controller = new NationalFreeDaysController(nationalFreeDaysData.Object);

            // act 
            var result = controller.AddFreeDay(freeDay);

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<IEnumerable<string>>(viewResult.ViewData.Model);

            model.Count().Should().BeGreaterThan(0);
        }

        [Fact]
        public void Delete_GoodInput_ReturnRedirectToAction()
        {
            NationalFreeDay freeDay = new NationalFreeDay();
            freeDay.Date = DateTime.MinValue;

            var nationalFreeDaysData = new Mock<INationalFreeDaysData>();
            nationalFreeDaysData.Setup(x => x.Delete(It.IsAny<int>()));      
            var controller = new NationalFreeDaysController(nationalFreeDaysData.Object);

            // act 
            var result = controller.Delete(55);

            //assert
            var viewResult = result.Should().BeOfType<RedirectToActionResult>().Subject; 
        }

        [Fact]
        public void Delete_IdNotFound_ReturnNotFound()
        {
            NationalFreeDay freeDay = new NationalFreeDay();
            freeDay.Date = DateTime.MinValue;

            var nationalFreeDaysData = new Mock<INationalFreeDaysData>();
            nationalFreeDaysData.Setup(x => x.Get(It.IsAny<int>())).Returns((NationalFreeDay)null); 
            var controller = new NationalFreeDaysController(nationalFreeDaysData.Object);
             
            // act 
            var result = controller.Delete(299);

            //assert
            var viewResult = result.Should().BeOfType<NotFoundResult>().Subject;
        }
    }
}
