﻿using Moq;
using VacationDays.Services;
using VacationDays.Controllers;
using VacationDays.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using System.Collections.Generic;
using FluentAssertions;
using System.Linq;
using VacationDays.ViewModels.Vacations;
using System;

namespace VacationDaysUnitTests
{
    public class VacationControllerUnitTests 
    {
        Mock<IVacationData> vacationServiceMock = new Mock<IVacationData>();
        Mock<IUserInfo> userInfoMock = new Mock<IUserInfo>();
        Mock<INationalFreeDaysData> nationalFreeDaysMock = new Mock<INationalFreeDaysData>();
        DateHelper dateHelper;
        Mock<IUserFreeDaysInfoData> userFreeDaysInfoData = new Mock<IUserFreeDaysInfoData>();

        private void InitializeMocks()
        {
            vacationServiceMock = new Mock<IVacationData>();
            userInfoMock = new Mock<IUserInfo>();
            nationalFreeDaysMock = new Mock<INationalFreeDaysData>();
            userFreeDaysInfoData = new Mock<IUserFreeDaysInfoData>();
            dateHelper = new DateHelper(nationalFreeDaysMock.Object);
        }

        [Fact]
        public void Index_NoCondition_ReturnsAllVacations()
        {
            // arrange
            InitializeMocks();

            vacationServiceMock.Setup(x => x.GetAll(userInfoMock.Object)).Returns(() => new List<Vacation>
            {
                new Vacation { Id = 1, OwnerId = "1" },
                new Vacation { Id = 2, OwnerId = "2" },
                new Vacation { Id = 3, OwnerId = "3" },
            });

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);
              
            // act
            var result = controller.Index();

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<IEnumerable<Vacation>>(viewResult.ViewData.Model);

            model.Count().Should().Be(3);  
        }

        [Fact]
        public void Details_GoodIdPassed_ReturnViewModel()
        {
            // arrange
            InitializeMocks();
            string ownerId = "TestUser";          

            var vacation = new Vacation() { Id = 99, OwnerId = ownerId };

            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(vacation); 

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);

            // act
            var result = controller.Details(33); 

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<Vacation>(viewResult.ViewData.Model);

            model.Id.Should().Be(99);
            model.OwnerId.Should().Be(ownerId); 
        }

        [Fact]
        public void Details_BadIdPassed_ReturnNotFound()
        {
            // arrange
            InitializeMocks();

            vacationServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns((Vacation)null);

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);

            // act
            var result = controller.Details(33);

            //assert
            var viewResult = result.Should().BeOfType<NotFoundObjectResult>().Subject;  
        }

        [Fact]
        public void Create_NoCondition_ReturnsView()
        {
            // arrange 
            InitializeMocks();

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);

            // act
            var result = controller.Create();

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
        }

        [Fact]
        public void Create_GoodInput_ReturnsRedirect()
        {
            // arrange 
            InitializeMocks();

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);

            var editModel = new VacationDateTimeEditModel();

            // act
            var result = controller.Create(editModel);

            //assert
            var viewResult = result.Should().BeOfType<RedirectToActionResult>().Subject;
        }

        [Fact]
        public void Create_NullInput_ReturnsErrorView()
        {
            // arrange 
            InitializeMocks();

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);

            // act
            var result = controller.Create((VacationDateTimeEditModel)null);

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
        }

        [Fact]
        public void Create_StartDateAfterEndDate_ReturnErrorView()
        {
            VacationDateTimeEditModel editModel = new VacationDateTimeEditModel();
            editModel.StartDate = DateTime.Now.Date;
            editModel.EndDate = DateTime.Now.Date.AddDays(-2);

            InitializeMocks();

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);
                 
            // act
            var result = controller.Create(editModel);

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<IEnumerable<string>>(viewResult.ViewData.Model);
            model.Count().Should().BeGreaterThan(0);
        }

        [Fact]
        public void Create_NoWorkingDaySelected_ReturnErrorView()
        {
            VacationDateTimeEditModel editModel = new VacationDateTimeEditModel();
            // set starting date to saturday
            editModel.StartDate = new DateTime(2019, 2, 2);
            // set ending date to sunday, same weekend
            editModel.EndDate = new DateTime(2019, 2, 3);

            InitializeMocks();

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);

            // act
            var result = controller.Create(editModel);

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<IEnumerable<string>>(viewResult.ViewData.Model);
            model.Count().Should().BeGreaterThan(0);
        }

        [Fact]
        public void ShowFreeDays_NoCondition_ReturnsViewModel()
        {
            // arrange
            string ownerId = "TestUser";
            InitializeMocks();

            vacationServiceMock.Setup(x => x.GetAll(userInfoMock.Object)).Returns(() => new List<Vacation>
            {
                new Vacation { Id = 1, OwnerId = ownerId },
                new Vacation { Id = 2, OwnerId = ownerId },
                new Vacation { Id = 3, OwnerId = ownerId },
            });

            UserFreeDaysInfo userFreeDaysInfo = new UserFreeDaysInfo();
            userFreeDaysInfo.FreeDaysAsBonusFromCompany = 55; 
            userFreeDaysInfoData.Setup(x => x.GetByUserID(It.IsAny<string>())).Returns(userFreeDaysInfo);

            var controller = new VacationController(vacationServiceMock.Object,
                userInfoMock.Object, nationalFreeDaysMock.Object, dateHelper, userFreeDaysInfoData.Object);

            // act
            var result = controller.ShowFreeDays();

            //assert
            var viewResult = result.Should().BeOfType<ViewResult>().Subject;
            var model = Assert.IsAssignableFrom<MyFreeDaysViewModel>(viewResult.ViewData.Model);
            model.BonusDaysFromCompany.Should().Be(55); 
        }

    }
}

 