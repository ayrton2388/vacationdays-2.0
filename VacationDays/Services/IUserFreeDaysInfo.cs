﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Models;

namespace VacationDays.Services
{
    public interface IUserFreeDaysInfoData
    {
        IEnumerable<UserFreeDaysInfo> GetAll();
        UserFreeDaysInfo GetByUserID(string userId);
        UserFreeDaysInfo Add(string userId, int freeDaysRequiredByLaw, int bonusDaysFromCompany, DateTime joinedCompanyDate);
        int GetFreeDaysGivenForYear(string userId,int year);
        int Test(); 
    }
}
