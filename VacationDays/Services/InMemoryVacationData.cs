﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Models;

namespace VacationDays.Services
{
    public class InMemoryVacationData : IVacationData
    {
        List<Vacation> _vacations;

        public InMemoryVacationData()
        {
            _vacations = new List<Vacation>()
            {
                new Vacation{ Id = 1, OwnerId = "1", StartDate = new DateTime(2018, 12, 20), EndDate = new DateTime(2018, 12,24) },
                new Vacation{ Id = 2, OwnerId = "2", StartDate = new DateTime(2018, 11, 4), EndDate = new DateTime(2018, 11,21) },
                new Vacation{ Id = 3, OwnerId = "1", StartDate = new DateTime(2018, 11, 1), EndDate = new DateTime(2018, 11,15) }
            };

        }

        public IEnumerable<Vacation> GetAll()
        {
            return _vacations.OrderBy(r => r.StartDate);
        }

        public Vacation Get(int id)
        {
            return _vacations.FirstOrDefault(r => r.Id == id);
        }

        public Vacation Add(Vacation newVacation)
        {
            newVacation.Id = _vacations.Max(r => r.Id) + 1;
            _vacations.Add(newVacation);
            return newVacation; 
        }

        public IEnumerable<Vacation> GetAll(IUserInfo userInfo)
        {
            throw new NotImplementedException();
        }

        public void Approve(int id)
        {
            throw new NotImplementedException();
        }

        public void Reject(int id)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
