﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks; 

namespace VacationDays.Services
{
    public class SmtpEmailService : IEmailService
    {
        IHttpContextAccessor _httpContextAccessor; 
        public SmtpEmailService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor; 
        }

        public async Task SendEmail(string expeditor, string destination, string title, string content)
        {
            using (var smtpClient = _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<SmtpClient>())
            {
                smtpClient.EnableSsl = true;
                smtpClient.TargetName = "STARTTLS/smtp.gmail.com";
                await smtpClient.SendMailAsync(new MailMessage(
                       from: expeditor,
                       to: destination,
                       subject: title,
                       body: content
                       )); 
            } 
        }
    }
}
