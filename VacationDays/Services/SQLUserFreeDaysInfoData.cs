﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Data;
using VacationDays.Models;

namespace VacationDays.Services
{
    public class SQLUserFreeDaysInfoData : IUserFreeDaysInfoData
    {
        private ApplicationDbContext _context;

        public SQLUserFreeDaysInfoData(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public UserFreeDaysInfo Add(string userId, int freeDaysRequiredByLaw, int bonusDaysFromCompany, DateTime joinDate)
        {
            UserFreeDaysInfo info = new UserFreeDaysInfo();
            info.OwnerId = userId;
            info.FreeDaysRequiredByLaw = freeDaysRequiredByLaw; 
            info.FreeDaysAsBonusFromCompany = bonusDaysFromCompany;
            info.JoinedCompanyDate = joinDate;
            _context.UserFreeDaysInfos.Add(info);
            _context.SaveChanges();
            return info;
        }

        private UserFreeDaysInfo Add(UserFreeDaysInfo userFreeDaysInfo)
        {
            _context.UserFreeDaysInfos.Add(userFreeDaysInfo);
            _context.SaveChanges();
            return userFreeDaysInfo; 
        }

        public IEnumerable<UserFreeDaysInfo> GetAll()
        {
            return _context.UserFreeDaysInfos.OrderByDescending(r => r.OwnerId);
        }

        public UserFreeDaysInfo GetByUserID(string userId)
        {
            return _context.UserFreeDaysInfos.FirstOrDefault(r => r.OwnerId == userId);
        }

        public int Test()
        {
            return _context.UserFreeDaysInfos.Count();
        } 

        public int GetFreeDaysGivenForYear(string userId, int year)
        {
            UserFreeDaysInfo info = GetByUserID(userId);
            return info.FreeDaysRequiredByLaw + info.FreeDaysAsBonusFromCompany+ (year - info.JoinedCompanyDate.Year);
        }
    }
}
