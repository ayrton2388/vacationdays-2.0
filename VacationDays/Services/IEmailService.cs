﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Services
{
    public interface IEmailService
    {
        Task SendEmail(string expeditor, string destination, string title, string content);
    }
}
