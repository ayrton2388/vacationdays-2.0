﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Classes;
using VacationDays.Data;
using VacationDays.Models;

namespace VacationDays.Services
{
    public class SqlVacationData : IVacationData
    {
        private ApplicationDbContext _context;

        public SqlVacationData(ApplicationDbContext context)
        {
            _context = context;
        }

        public Vacation Add(Vacation newVacation)
        {
            _context.Vacations.Add(newVacation);
            _context.SaveChanges();
            return newVacation;
        }

        public void Remove(int id) 
        {
            var existing = _context.Vacations.First(x => x.Id == id);
            _context.Vacations.Remove(existing);
            _context.SaveChanges(); 
        }

        public Vacation Get(int id)
        {
            return _context.Vacations.FirstOrDefault(r => r.Id == id); 
        }

        public IEnumerable<Vacation> GetAll()
        {
            return _context.Vacations.OrderByDescending(r => r.StartDate); 
        }

        public IEnumerable<Vacation> GetAll(IUserInfo userInfo)
        {
            return _context.Vacations.Where(r => r.OwnerId == userInfo.GetUserID()).OrderByDescending(r => r.StartDate); 
        }

        public void Approve(int id)
        {
            Get(id).Status = VacationStatus.Approved;
            _context.SaveChanges();
        }

        public void Reject(int id)
        {
            Get(id).Status = VacationStatus.Rejected;
            _context.SaveChanges();
        }
    }
}
