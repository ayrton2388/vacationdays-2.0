﻿using Microsoft.Extensions.Configuration;

namespace VacationDays.Services
{
    public interface IGreeter
    {
        string GetMessageOfTheDay();
    }

    public class Greeter : IGreeter
    {
        public string GetMessageOfTheDay()
        {
            return "Greetings from Greeter Service!"; 
        }
    }

    public class GreeterDependencyInjection : IGreeter
    {
        private IConfiguration _configuration;

        public GreeterDependencyInjection(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GetMessageOfTheDay()
        {
            return _configuration["Greeting"];
        }
    }
}