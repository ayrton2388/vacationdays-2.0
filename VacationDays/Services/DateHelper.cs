﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Services
{
    public class DateHelper
    {
        private INationalFreeDaysData _nationalFreeDaysData;

        public DateHelper(INationalFreeDaysData nationalFreeDaysData)
        {
            _nationalFreeDaysData = nationalFreeDaysData;
        }

        public int GetWorkingDays(DateTime startDate, DateTime endDate)
        {
            var totalDays = 0;
            for (var date = startDate; date <= endDate; date = date.AddDays(1))
            {
                if (IsWorkingDay(date))
                    totalDays++;
            }

            return totalDays;
        }

        public bool IsWorkingDay(DateTime date)
        {
            if (date.DayOfWeek != DayOfWeek.Saturday
                   && date.DayOfWeek != DayOfWeek.Sunday
                   && !_nationalFreeDaysData.IsNationalFreeDay(date))
                return true;
            return false;
        }
    }
}
