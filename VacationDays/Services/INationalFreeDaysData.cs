﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Models;

namespace VacationDays.Services
{
    public interface INationalFreeDaysData
    {
        IEnumerable<NationalFreeDay> GetAll();
        NationalFreeDay Get(int id); 
        bool IsNationalFreeDay(DateTime date);
        NationalFreeDay Add(NationalFreeDay nationalFreeDay);
        void Delete(int id); 
    }
}
