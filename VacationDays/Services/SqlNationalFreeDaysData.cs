﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Data;
using VacationDays.Models;

namespace VacationDays.Services
{
    public class SqlNationalFreeDaysData : INationalFreeDaysData
    {
        private ApplicationDbContext _context;

        public SqlNationalFreeDaysData(ApplicationDbContext context)
        {
            _context = context;
        }

        public NationalFreeDay Add(NationalFreeDay nationalDay)
        {
            NationalFreeDay newFreeDay = nationalDay;
            _context.NationalFreeDays.Add(newFreeDay);
            _context.SaveChanges();
            return newFreeDay;
        }

        public void Delete(int id)
        {
            _context.NationalFreeDays.Remove(Get(id));
            _context.SaveChanges();  
        }

        public NationalFreeDay Get(int id)
        {
            return _context.NationalFreeDays.First(r => r.Id == id);
        }

        public IEnumerable<NationalFreeDay> GetAll()
        {
            return _context.NationalFreeDays.OrderByDescending(r => r.Date);
        }

        public bool IsNationalFreeDay(DateTime date)
        {
            foreach (NationalFreeDay nfd in _context.NationalFreeDays)
            {
                if (nfd.Date == date)
                    return true;
            }

            return false;
        }
    }
}
