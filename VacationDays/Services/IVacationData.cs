﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Models;

namespace VacationDays.Services
{
    public interface IVacationData
    {
        IEnumerable<Vacation> GetAll();
        IEnumerable<Vacation> GetAll(IUserInfo userInfo);
        Vacation Get(int id);
        Vacation Add(Vacation newVacation);
        void Remove(int id);
        void Approve(int id);
        void Reject(int id);
    }
}
