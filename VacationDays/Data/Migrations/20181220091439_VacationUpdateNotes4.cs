﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationDays.Data.Migrations
{
    public partial class VacationUpdateNotes4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Notes",
                table: "Vacations");

            migrationBuilder.DropColumn(
                name: "Notes2",
                table: "Vacations");

            migrationBuilder.DropColumn(
                name: "OwnerId2",
                table: "Vacations");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Notes",
                table: "Vacations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notes2",
                table: "Vacations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OwnerId2",
                table: "Vacations",
                nullable: false,
                defaultValue: 0);
        }
    }
}
