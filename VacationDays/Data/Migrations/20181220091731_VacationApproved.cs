﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationDays.Data.Migrations
{
    public partial class VacationApproved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Vacations",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Vacations");
        }
    }
}
