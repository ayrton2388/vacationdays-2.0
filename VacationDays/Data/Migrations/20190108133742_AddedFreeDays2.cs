﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationDays.Data.Migrations
{
    public partial class AddedFreeDays2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NationalFreeDays",
                columns: table => new
                {
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NationalFreeDays", x => x.Date);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NationalFreeDays");
        }
    }
}
