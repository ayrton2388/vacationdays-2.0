﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationDays.Data.Migrations
{
    public partial class VacationStatusAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Vacations");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Vacations",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Vacations");

            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Vacations",
                nullable: false,
                defaultValue: false);
        }
    }
}
