﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationDays.Data.Migrations
{
    public partial class AddedFreeDays3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NationalFreeDays",
                table: "NationalFreeDays");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "NationalFreeDays",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NationalFreeDays",
                table: "NationalFreeDays",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NationalFreeDays",
                table: "NationalFreeDays");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "NationalFreeDays");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NationalFreeDays",
                table: "NationalFreeDays",
                column: "Date");
        }
    }
}
