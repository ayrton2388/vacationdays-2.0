﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VacationDays.Models;

namespace VacationDays.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {

        // needed for unit testing
        public ApplicationDbContext() : base()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        // "virtual" needed for unit testing
        public virtual DbSet<VacationDays.Models.Vacation> Vacations { get; set; }
        public virtual DbSet<VacationDays.Models.NationalFreeDay> NationalFreeDays { get; set; }
        public virtual DbSet<VacationDays.Models.UserFreeDaysInfo> UserFreeDaysInfos { get; set; }
    }
}
