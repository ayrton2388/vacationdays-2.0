﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Data
{
    public static class RolesInitializer
    {
        public static async Task InitializeRolesAsync(ApplicationDbContext context, IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            string[] roleNames = { "Admin", "Manager", "Member" };

            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExists = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExists)
                {
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName)); 
                }
            }

            await CreateSuperUser(serviceProvider); 
        }

        private static async Task CreateSuperUser(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
 
            var adminEmail = "admin@evozon.com";
            var adminPassword = "Admin1234!";

            IdentityUser testUser = await userManager.FindByEmailAsync(adminEmail); 

            if (testUser == null)
            {
                System.Diagnostics.Debug.WriteLine("");
                System.Diagnostics.Debug.WriteLine("=====================");
                System.Diagnostics.Debug.WriteLine("creating super user");
                System.Diagnostics.Debug.WriteLine("=====================");
                System.Diagnostics.Debug.WriteLine("");

                IdentityUser admin = new IdentityUser();
                admin.Email = adminEmail;
                admin.UserName = adminEmail;  
                admin.SecurityStamp = Guid.NewGuid().ToString();

                IdentityResult newUser = await userManager.CreateAsync(admin, adminPassword);
                await userManager.AddToRoleAsync(admin, "Admin");
                await userManager.AddToRoleAsync(admin, "Manager");                    
            }             
        }
    }
}
