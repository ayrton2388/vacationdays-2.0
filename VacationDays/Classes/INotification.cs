﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Classes
{
    public interface INotification
    {
        int GetValue();
    }

}
