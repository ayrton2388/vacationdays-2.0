﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Services;

namespace VacationDays.Classes
{
    public class PendingRequestsNotification : INotification
    {
        private IVacationData _vacationData;

        public PendingRequestsNotification(IVacationData vacationData)
        {
            _vacationData = vacationData;
        }

        public int GetValue()
        {
            return _vacationData.GetAll().Where(r => r.Status == VacationStatus.Pending).Count();
        }
    }
}
