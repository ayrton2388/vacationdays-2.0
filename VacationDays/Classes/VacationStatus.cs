﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Classes
{
    public enum VacationStatus
    {
        Pending = 0, 
        Approved = 1,
        Rejected =2
    }
}
