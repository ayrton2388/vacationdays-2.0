﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationDays.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Routing;
using VacationDays.Services;
using System.Net.Mail;
using System.Net;
using VacationDays.Classes;

namespace VacationDays
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<SmtpClient>((serviceProvider) =>
            {
                var config = serviceProvider.GetRequiredService<IConfiguration>();
                return new SmtpClient()
                {
                    Host = config.GetValue<String>("Email:Smtp:Host"),
                    Port = config.GetValue<int>("Email:Smtp:Port"),
                    EnableSsl = true, 
                    Credentials = new NetworkCredential(
                            config.GetValue<String>("Email:Smtp:Username"),
                            config.GetValue<String>("Email:Smtp:Password")
                        )
                };
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddDefaultIdentity<IdentityUser>()
                .AddRoles<IdentityRole>().AddRoleManager<RoleManager<IdentityRole>>()    // !!! Add identity roles and manager!
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddHttpContextAccessor(); // so i can acces user info, like ID, name, etc
            services.AddScoped<IUserInfo, UserInfo>(); 

            services.AddSingleton<IGreeter, GreeterDependencyInjection>();
            services.AddScoped<IVacationData, SqlVacationData>();
            services.AddScoped<INationalFreeDaysData, SqlNationalFreeDaysData>();
            services.AddScoped<DateHelper>();
            services.AddScoped<IUserFreeDaysInfoData, SQLUserFreeDaysInfoData>();
            services.AddScoped<PendingRequestsNotification>();
            services.AddScoped<NotificationsService>();
            services.AddScoped<SmtpEmailService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
                                IHostingEnvironment env,
                                IGreeter greeter,
                                ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();



            //app.Use(next =>
            //{
            //    #region Custom piece of middleware. Example.
            //    return async context =>
            //    {
            //        logger.LogInformation("Request incoming");
            //        if (context.Request.Path.StartsWithSegments("/mym"))
            //        {
            //            await context.Response.WriteAsync("Hit!");
            //            logger.LogInformation("Request handled");
            //            await context.Response.WriteAsync("Hit2!");
            //            await next(context);
            //        }
            //        else
            //        {
            //            await next(context);
            //            logger.LogInformation("Response outgoing");
            //        }
            //    };
            //    #endregion
            //});

            //app.UseWelcomePage(new WelcomePageOptions { Path = "/wp" });

            app.UseMvc(ConfigureRoutes);

            app.Run( async (context) =>
            {
                await context.Response.WriteAsync("Page not found!");

            });

        }

        private void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Default", "{controller=Home}/{action=Index}/{id?}");
        }
               
    }
}
