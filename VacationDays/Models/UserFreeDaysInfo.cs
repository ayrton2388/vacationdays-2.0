﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Models
{
    public class UserFreeDaysInfo
    {
        public int Id { get; set; }
        public string OwnerId { get; set; }

        [System.ComponentModel.DefaultValue(25)]
        public int FreeDaysRequiredByLaw { get; set; }

        public int FreeDaysAsBonusFromCompany { get; set; }
        public DateTime JoinedCompanyDate { get; set; }
    }
}
