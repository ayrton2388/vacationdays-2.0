﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Models
{
    public class NationalFreeDay
    {
        public NationalFreeDay()
        {
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}
