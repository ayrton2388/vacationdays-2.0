﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Classes;

namespace VacationDays.Models
{
    public class Vacation
    {
        public int Id { get; set; }
        public string OwnerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public VacationStatus Status { get; set; }
        public int DaysTaken { get; set; }
    }
}
