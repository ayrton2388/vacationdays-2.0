﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Models;

namespace VacationDays.ViewModels
{
    public class VacationManagementOutputModel
    {
        public List<VacationPresentation> vacationsList;

        public VacationManagementOutputModel()
        {
            vacationsList = new List<VacationPresentation>();
        }        
    }

    public class VacationPresentation : Vacation
    {
        public string OwnerName;
    }
}
