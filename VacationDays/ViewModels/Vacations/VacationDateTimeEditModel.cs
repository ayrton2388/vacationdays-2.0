﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using VacationDays.Data.Validation;

namespace VacationDays.ViewModels.Vacations
{
    public class VacationDateTimeEditModel
    {
        [Display(Name = "Start date")]
        [Required]
        public DateTime StartDate { get; set; }

        [Display(Name = "End date")]
        [Required]
        public DateTime EndDate { get; set; }
    }
}