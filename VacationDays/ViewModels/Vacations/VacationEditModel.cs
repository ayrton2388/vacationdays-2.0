﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations; 

namespace VacationDays.ViewModels.Vacations
{
    public class VacationEditModel
    {
        [Display(Name = "Start date: day")]
        [Required, Range(1,31)]
        public int StartDateDay { get; set; }

        [Display(Name = "Start date: month")]
        [Required][Range(1,12)]
        public int StartDateMonth { get; set; }

        [Display(Name = "Start date: year")]
        [Required]
        public int StartDateYear { get; set; }

        [Display(Name = "End date: day")]
        [Required, Range(1, 31)]
        public int EndDateDay { get; set; }

        [Required]
        [Range(1, 12)]
        [Display(Name = "End date: month")]       
        public int EndDateMonth { get; set; }

        [Display(Name = "End date: year")]
        [Required]
        public int EndDateYear { get; set; } 
    }
}
  