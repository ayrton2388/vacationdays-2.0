﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.ViewModels.Vacations
{
    public class MyFreeDaysViewModel
    {
        public DateTime DateHired { get; set; }
        public int FreeDaysRequiredByLaw { get; set; }
        public int BonusDaysFromCompany { get; set; }
        public int FreeDaysGivenThisYear { get; set; }
        public int FreeDaysLeftFromLastYear { get; set; }
        public int VacationDaysTakenThisYear { get; set; }
        public int TotalAvailableFreeDays { get; set; }
        public SortedDictionary<int, int> YearlyHistory { get; set; }

    }
}
