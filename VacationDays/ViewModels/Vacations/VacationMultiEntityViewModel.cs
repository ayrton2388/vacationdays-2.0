﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationDays.Models;

namespace VacationDays.ViewModels
{
    public class VacationMultiEntityViewModel
    {
        public IEnumerable<Vacation> Vacations { get; set; }
        public string CurrentMessage { get; set; }
    }
}
