﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using VacationDays.Models;
using VacationDays.Services;

namespace VacationDays.Controllers
{
    /// <summary>
    /// This class is used for learning / testing only
    /// </summary>

    public class UserController : Controller
    {
        private IVacationData _vacationData;
        private IUserInfo _userInfo;
        private UserManager<IdentityUser> _userManager;

        public UserController(IVacationData vacationData, IUserInfo userInfo, UserManager<IdentityUser> userManager)
        {
            _vacationData = vacationData;
            _userInfo = userInfo;
            _userManager = userManager; 
        }

        public IActionResult Index()
        {

            return Content(User.Identity.Name);
        }

        private Task<IdentityUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);

        public async Task<IActionResult> Roles()
        {
            var user = await GetCurrentUserAsync();
            var roles = await _userManager.GetRolesAsync(user);

            string rolesString = "";
            foreach (string role in roles)
                rolesString += " " + role;


            if (_userInfo.IsAuthenticated())
                return Content(rolesString);               
            else
                return Redirect("/Identity/Account/Register");  
        }

        public async Task<IActionResult> Claims()
        {
            var user = await GetCurrentUserAsync();
            var roles = await _userManager.GetRolesAsync(user);

            string rolesString = "";

            foreach (Claim role in User.Claims)
                rolesString += " " + role.ToString();

            if (_userInfo.IsAuthenticated())
                return Content(rolesString);
            //return Content(_userInfo.GetUserID() + " has admin role: " + User.IsInRole("Manager"));
            else
                return Redirect("/Identity/Account/Register");
        }

    }
}
