﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationDays.Controllers
{
    /// <summary>
    /// This class is used for learning / testing only
    /// </summary>

    [Route("admin/[controller]/[action]")]
    public class TestController
    {
        public string Index()
        {
            return "Hello from test controller";
        }

        public string Phone()
        {
            return "1+555+555+555";
        }

        public string Address()
        {
            return "USA";
        }
    }
}
