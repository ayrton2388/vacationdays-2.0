﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VacationDays.Models;
using VacationDays.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationDays.Controllers
{
    [Authorize (Roles = "Manager,Admin")]
    public class NationalFreeDaysController : Controller
    {
        private INationalFreeDaysData _nationalFreeDaysData;

        public NationalFreeDaysController(INationalFreeDaysData nationalFreeDaysData)
        {
            _nationalFreeDaysData = nationalFreeDaysData;

        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var model = _nationalFreeDaysData.GetAll();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]  // pretty good practice for POST functions
        public IActionResult AddFreeDay(NationalFreeDay freeDay)
        {
            //return Content(freeDay.Date.ToShortDateString()); 

            List<string> errors = new List<string>();

            if (freeDay==null)
            {
                string nullInput = "Input cannot be null!";
                errors.Add(nullInput);
                return View("AddFreeDayError", errors);
            }

            if (_nationalFreeDaysData.IsNationalFreeDay(freeDay.Date))
            {
                string errorItemAlreadyExists = "The selected date already exists!";
                errors.Add(errorItemAlreadyExists);
            }

            if (freeDay.Date == DateTime.MinValue)
            {
                string errorChangeDefaultValue = "Please set a date value.";
                errors.Add(errorChangeDefaultValue); 
            }

            if (ModelState.IsValid && errors.Count == 0)
            {
                _nationalFreeDaysData.Add(freeDay);
                // After POSTing, its good practice to RE-DIRECT to something, so it wont POST again on a page refresh.
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View("AddFreeDayError",errors);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]  // pretty good practice for POST functions
        public IActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                var dayToDelete = _nationalFreeDaysData.Get(id);

                if (dayToDelete == null)
                {
                    return NotFound();
                }

                _nationalFreeDaysData.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            else
                return BadRequest();            
        }


    }
}
