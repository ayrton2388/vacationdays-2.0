﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationDays.Data;
using VacationDays.Models;
using VacationDays.Services;

namespace VacationDays.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VacationsApiController : ControllerBase
    {
        private IVacationData _vacationService;

        public VacationsApiController(IVacationData vacationDataService)
        {
            _vacationService = vacationDataService; 
        }

        [HttpGet]
        public IActionResult GetAllVacations()
        {
            var result =  _vacationService.GetAll();
            return Ok(result);  
        }         

        // GET: api/VacationsApi/5
        [HttpGet("{id}")]
        public IActionResult GetVacation(int id) 
        {
            var vacation = _vacationService.Get(id);

            if (vacation == null)
            {
                return NotFound(); 
            }
             
            return Ok(vacation); 
        }

        // POST: api/VacationsApi
        [HttpPost]
        public IActionResult Create(Vacation vacation)
        {
            if (vacation == null)
            {
                return BadRequest();
            }
            try
            {
                _vacationService.Add(vacation);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return Ok(); 
        }

        [Route("api/[controller]/createandreturn")] 
        [HttpPost]
        public IActionResult CreateAndReturn(Vacation vacation) 
        {
            if (vacation == null)
            {
                return BadRequest();
            }
            
            Vacation newVacation = _vacationService.Add(vacation);
            return Ok(newVacation); 
        }

        // DELETE: api/VacationsApi/5
        [HttpDelete("{id}")]
        public IActionResult DeleteVacation(int id)
        {
            var vacation = _vacationService.Get(id);

            if (vacation == null)
            {
                return NotFound();
            }

            _vacationService.Remove(vacation.Id);
            return Ok();
        } 
    }
}
