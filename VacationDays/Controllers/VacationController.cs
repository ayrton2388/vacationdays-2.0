﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VacationDays.Classes;
using VacationDays.Models;
using VacationDays.Services;
using VacationDays.ViewModels;
using VacationDays.ViewModels.Vacations;

namespace VacationDays.Controllers
{
    [Authorize]
    public class VacationController : Controller
    {
        private DateHelper _dateHelper;
        private IVacationData _vacationData;
        private IUserInfo _userInfo;
        private INationalFreeDaysData _nationalFreeDaysData;
        private IUserFreeDaysInfoData _userFreeDaysInfoData;

        public VacationController(IVacationData vacationData, IUserInfo userInfo, INationalFreeDaysData nationalFreeDaysData,
            DateHelper dateHelper, IUserFreeDaysInfoData userFreeDaysInfoData)
        {
            _dateHelper = dateHelper;
            _vacationData = vacationData;
            _userInfo = userInfo;
            _nationalFreeDaysData = nationalFreeDaysData;
            _userFreeDaysInfoData = userFreeDaysInfoData;
        }

        public IActionResult Index()
        {
            var model = _vacationData.GetAll(_userInfo); 
            return View(model); 
        }

        public IActionResult Details(int id)
        {
            var model = _vacationData.Get(id);

            if (model != null)
                return View(model);
            else 
                return NotFound("Vacation is not found!!!"); 
        }


        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]  // pretty good practice for POST functions
        public IActionResult Create(VacationDateTimeEditModel model)
        {
            List<string> errors = CheckVacationCreationForErrors(model);

            if (ModelState.IsValid && errors.Count == 0 && model != null)
            {
                var newVacation = new Vacation();
                newVacation.Status = VacationStatus.Pending;
                newVacation.OwnerId = _userInfo.GetUserID(); 
                newVacation.StartDate = model.StartDate;
                newVacation.EndDate = model.EndDate;
                newVacation.DaysTaken = _dateHelper.GetWorkingDays(newVacation.StartDate, newVacation.EndDate);
                newVacation = _vacationData.Add(newVacation);

                // After POSTing, its good practice to RE-DIRECT to something, so it wont POST again on a page refresh.
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View("CreateError", errors);
            }
        }

        List<string> CheckVacationCreationForErrors(VacationDateTimeEditModel model)
        {
            List<string> errors = new List<string>();

            if (model == null)
            {
                string nullModel = "Model cannot be null";
                errors.Add(nullModel);
                return errors;
            }

            int daysTaken = _dateHelper.GetWorkingDays(model.StartDate, model.EndDate);

            if (model.EndDate >= model.StartDate && daysTaken == 0)
            {
                string errorZeroDays = "No working day was selected";
                errors.Add(errorZeroDays);                
            }

            if (model.EndDate < model.StartDate)
            {
                string errorNegativeDays = "Vacation cannot end before it starts.";
                errors.Add(errorNegativeDays);
            }

            return errors;
        }

        public IActionResult ShowFreeDays()
        {
            var vacationData = _vacationData.GetAll(_userInfo).Where(v => v.Status == VacationStatus.Approved).OrderBy(v => v.EndDate);

            var model = new MyFreeDaysViewModel();
            model.YearlyHistory = new SortedDictionary<int, int>();

            foreach (Vacation v in vacationData)
            {
                for (var date = v.StartDate; date <= v.EndDate; date = date.AddDays(1))
                {
                    if (_dateHelper.IsWorkingDay(date))
                    {
                        if (model.YearlyHistory.ContainsKey(date.Year))
                        {
                            model.YearlyHistory[date.Year] += 1;
                        }
                        else
                        {
                            model.YearlyHistory.Add(date.Year, 1);
                        }
                    }
                }
            }
            
            var userFreeDaysInfo = _userFreeDaysInfoData.GetByUserID(_userInfo.GetUserID());

            // if the user has no data, just create a default user free days info. This should never happen on the live app.
            // a userFreeDaysInfo should be created and saved in DB for each user when the user account is created. 
            if (userFreeDaysInfo == null)
            {
                userFreeDaysInfo = _userFreeDaysInfoData.Add(_userInfo.GetUserID(), 25, 1, DateTime.Now);
            }

            model.DateHired = userFreeDaysInfo.JoinedCompanyDate;
            model.FreeDaysRequiredByLaw = userFreeDaysInfo.FreeDaysRequiredByLaw;
            model.BonusDaysFromCompany = userFreeDaysInfo.FreeDaysAsBonusFromCompany;
            model.FreeDaysGivenThisYear = _userFreeDaysInfoData.GetFreeDaysGivenForYear(_userInfo.GetUserID(), DateTime.Now.Year);
           
            int daysTakenThisYear;
            model.YearlyHistory.TryGetValue(DateTime.Now.Year, out daysTakenThisYear);
            model.VacationDaysTakenThisYear = daysTakenThisYear;

            int daysTakenLastYear;
            model.YearlyHistory.TryGetValue(DateTime.Now.Year-1, out daysTakenLastYear);
            model.FreeDaysLeftFromLastYear = _userFreeDaysInfoData.GetFreeDaysGivenForYear(_userInfo.GetUserID(), DateTime.Now.Year - 1) - daysTakenLastYear;

            model.TotalAvailableFreeDays = model.FreeDaysGivenThisYear + model.FreeDaysLeftFromLastYear - daysTakenThisYear;

            return View("MyFreeDays", model);
        }

    }
}
