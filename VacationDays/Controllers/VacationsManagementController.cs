﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using VacationDays.Classes;
using VacationDays.Models;
using VacationDays.Services;
using VacationDays.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationDays.Controllers
{
    [Authorize(Roles = "Manager")]
    public class VacationsManagementController : Controller
    {
        private DateHelper _dateHelper;
        private IVacationData _vacationData;
        private IUserInfo _userInfo;
        private UserManager<IdentityUser> _userManager;
        private IEmailService _emailService;

        const string MAIL_EXPEDITOR = "andreilng41@gmail.com";
        const string CONFIRMATON_EMAIL_TITLE = "Vacation request approved";
        const string CONFIRMATON_EMAIL_BODY = "Your vacation has been approved by your manager.";
        const string REJECTION_EMAIL_TITLE = "Vacation request denied";
        const string REJECTION_EMAIL_BODY = "Your vacation request has been denied. Please contact your manager.";
        const string START_DATE = "Start date: ";
        const string END_DATE =   "End date:   ";
        const string WORKING_DAYS = "Working days: ";
        const string NEW_LINE = "\n";

        public VacationsManagementController(IVacationData vacationData, IUserInfo userInfo, UserManager<IdentityUser> userManager,
            DateHelper dateHelper, IEmailService emailService)
        {
            _dateHelper = dateHelper;
            _vacationData = vacationData;
            _userInfo = userInfo;
            _userManager = userManager;
            _emailService = emailService;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var vmo = new VacationManagementOutputModel();

            foreach (Vacation vacation in _vacationData.GetAll())
            {
                var vacationPresentation = new VacationPresentation();
                vacationPresentation.Id = vacation.Id;
                vacationPresentation.Status = vacation.Status;
                vacationPresentation.StartDate = vacation.StartDate;
                vacationPresentation.EndDate = vacation.EndDate;
                vacationPresentation.DaysTaken = vacation.DaysTaken;
                vacationPresentation.OwnerName = _userManager.Users.FirstOrDefault(user => user.Id == vacation.OwnerId).UserName;
                vmo.vacationsList.Add(vacationPresentation); 
            }

            return View(vmo);
        }       

        public IActionResult Pending()
        {
            var vmo = new VacationManagementOutputModel();

            foreach (Vacation vacation in _vacationData.GetAll().Where(r=> r.Status == VacationStatus.Pending))
            {
                var vacationPresentation = new VacationPresentation();
                vacationPresentation.Id = vacation.Id;
                vacationPresentation.Status = vacation.Status;
                vacationPresentation.StartDate = vacation.StartDate;
                vacationPresentation.EndDate = vacation.EndDate;
                vacationPresentation.DaysTaken = vacation.DaysTaken;
                vacationPresentation.OwnerName = _userManager.Users.First(user => user.Id == vacation.OwnerId).UserName;
                vmo.vacationsList.Add(vacationPresentation);
            }

            return View(vmo);
        }

        public IActionResult Details(int id)
        {
            var model = _vacationData.Get(id);

            if (model != null)
                return View("Details",model);
            else
                return NotFound("Vacation is not found!");
        }

        // !! set the param names to model fields names! otherwise it wont bind!
        public async Task<IActionResult> Approve(int id, bool sendEmail, string test)
        {             
            _vacationData.Approve(id);
            await SendEmail(id, CONFIRMATON_EMAIL_TITLE, CONFIRMATON_EMAIL_BODY);
            return RedirectToAction(nameof(Pending)); 
        }

        public async Task<IActionResult> Reject(int id, bool sendEmail)
        {
            _vacationData.Reject(id);
            await SendEmail(id, REJECTION_EMAIL_TITLE, REJECTION_EMAIL_BODY);
            return RedirectToAction(nameof(Pending));
        }

        public async Task<IActionResult> SendEmail(int id, string title, string content)
        {          
            string ownerId = _vacationData.Get(id).OwnerId;
            string targetEmail = _userManager.Users.First(user => user.Id == ownerId).Email;
          
            using (var smtpClient = HttpContext.RequestServices.GetRequiredService<SmtpClient>())
            {
                smtpClient.EnableSsl = true;
                smtpClient.TargetName = "STARTTLS/smtp.gmail.com";
                await smtpClient.SendMailAsync(new MailMessage(
                       from: MAIL_EXPEDITOR,
                       to: targetEmail,
                       subject: title,
                       body: BuildEmailContent(id, content)
                       ));

                return RedirectToAction(nameof(Pending));
            }
        }

        private string BuildEmailContent(int id, string bodyContent)
        {
            string content = bodyContent; 
            content += NEW_LINE;
            string sd = START_DATE + _vacationData.Get(id).StartDate.ToString("MMMM, dd, yyyy");
            content += sd;
            content += NEW_LINE;
            string ed = END_DATE + _vacationData.Get(id).EndDate.ToString("MMMM, dd, yyyy");
            content += ed;
            content += NEW_LINE;
            content += WORKING_DAYS + _dateHelper.GetWorkingDays(_vacationData.Get(id).StartDate, _vacationData.Get(id).EndDate);
            return content; 
        }

      
    }
}
